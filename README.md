**Nuget packaging for RapidJSON Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target rapidjson)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:rapidjson,INTERFACE_INCLUDE_DIRECTORIES>")
```